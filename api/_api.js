
export default {
  fetchData: async (target) => {
    return await fetch(`https://jsonplaceholder.typicode.com/${target}`).then(response => response.json())
  },
}